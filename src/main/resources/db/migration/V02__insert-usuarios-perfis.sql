INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Adm', 'adm@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario0', 'usuario0@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario1', 'usuario1@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario2', 'usuario2@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario3', 'usuario3@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario4', 'usuario4@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario5', 'usuario5@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123
INSERT INTO USUARIO(nome, email, senha, ativo) VALUES('Usuario6', 'usuario6@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq', true); -- senha eh 123


INSERT INTO PERFIL(id, nome) VALUES(1, 'ROLE_ADM');
INSERT INTO PERFIL(id, nome) VALUES(2, 'ROLE_USUARIO');

INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(1, 1);
INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(2, 2);
INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(3, 2);
INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(4, 2);
INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(5, 2);
INSERT INTO USUARIO_PERFIS(usuario_id, perfis_id) VALUES(6, 2);

INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 1', 'DIRETOR');
INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 2', 'ATOR');
INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 3', 'ATOR');
INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 4', 'ATOR');
INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 5', 'ATOR');
INSERT INTO `ioasys`.`integrante` (`nome`, `perfil`) VALUES ('Integrante 6', 'ATOR');
