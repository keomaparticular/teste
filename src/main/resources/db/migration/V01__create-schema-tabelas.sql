CREATE DATABASE  IF NOT EXISTS ioasys;

USE ioasys;

CREATE TABLE filme (
  id bigint NOT NULL AUTO_INCREMENT,
  descricao varchar(255) DEFAULT NULL,
  genero int DEFAULT NULL,
  nome varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE integrante (
  id bigint NOT NULL AUTO_INCREMENT,
  nome varchar(255) DEFAULT NULL,
  perfil varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE filme_integrantes (
  Filme_id bigint NOT NULL,
  integrantes_id bigint NOT NULL,
  KEY FK5p5r5nqs5gofjbwnw8iunkjbg (integrantes_id),
  KEY FKsb4nxs5topm58kt5u092de3e2 (Filme_id),
  CONSTRAINT FK5p5r5nqs5gofjbwnw8iunkjbg FOREIGN KEY (integrantes_id) REFERENCES integrante (id),
  CONSTRAINT FKsb4nxs5topm58kt5u092de3e2 FOREIGN KEY (Filme_id) REFERENCES filme (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE perfil (
  id bigint NOT NULL AUTO_INCREMENT,
  nome varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE usuario (
  id bigint NOT NULL AUTO_INCREMENT,
  email varchar(255) DEFAULT NULL,
  nome varchar(255) DEFAULT NULL,
  senha varchar(255) DEFAULT NULL,
  ativo TINYINT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE usuario_perfis (
  Usuario_id bigint NOT NULL,
  perfis_id bigint NOT NULL,
  KEY FKt8gpyoomg6nbpmwexxjmx9pkw (perfis_id),
  KEY FKbl3tsfnts24te4um6sam8y0in (Usuario_id),
  CONSTRAINT FKbl3tsfnts24te4um6sam8y0in FOREIGN KEY (Usuario_id) REFERENCES usuario (id),
  CONSTRAINT FKt8gpyoomg6nbpmwexxjmx9pkw FOREIGN KEY (perfis_id) REFERENCES perfil (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE voto (
  id bigint NOT NULL AUTO_INCREMENT,
  filme_id bigint DEFAULT NULL,
  usuario_id bigint DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FKavfncer660nxvriyq2mac2e5c (filme_id),
  KEY FKqpto5w6ol6co2iurosvbbp6gx (usuario_id),
  CONSTRAINT FKavfncer660nxvriyq2mac2e5c FOREIGN KEY (filme_id) REFERENCES filme (id),
  CONSTRAINT FKqpto5w6ol6co2iurosvbbp6gx FOREIGN KEY (usuario_id) REFERENCES usuario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;