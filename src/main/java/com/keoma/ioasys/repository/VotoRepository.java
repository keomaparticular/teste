package com.keoma.ioasys.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.keoma.ioasys.model.Filme;
import com.keoma.ioasys.model.Usuario;
import com.keoma.ioasys.model.Voto;

public interface VotoRepository extends JpaRepository<Voto, Long>  {
	
	@Query(value = "SELECT v FROM Voto v join v.usuario u join v.filme f  WHERE v.filme = :filme AND v.usuario = :usuario")
	Optional<Voto> findByUsuarioAndFilme(Usuario usuario, Filme filme);

}
