package com.keoma.ioasys.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.keoma.ioasys.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	Optional<Usuario> findByEmail(String email);
		
	@Query(value = "SELECT u FROM Usuario u join u.perfis up WHERE u.ativo = true AND up.nome = :perfil order by u.nome")
	Page<Usuario> findUsuariosAtivos(String perfil, Pageable pageable);	

}
