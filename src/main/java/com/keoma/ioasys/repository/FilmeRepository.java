package com.keoma.ioasys.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.keoma.ioasys.model.Filme;
import com.keoma.ioasys.model.enums.GeneroEnum;

public interface FilmeRepository extends JpaRepository<Filme, Long> {

	@Query("select f from Filme f "			
			+ " where 1=1"			
	        + " and (:nome is null or f.nome = :nome)"
	        + " and (:descricao is null or f.descricao = :descricao)"
	        + " and (:genero is null or f.genero = :genero)"	        
			)
	Page<Filme> findByFilter(String nome, String descricao, GeneroEnum genero, Pageable pageable);

}
