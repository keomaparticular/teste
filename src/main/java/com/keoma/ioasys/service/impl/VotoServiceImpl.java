package com.keoma.ioasys.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keoma.ioasys.controller.dto.VotoSaveDto;
import com.keoma.ioasys.exception.FilmeNaoEncontradoException;
import com.keoma.ioasys.exception.UsuarioNaoEncontradoException;
import com.keoma.ioasys.model.Filme;
import com.keoma.ioasys.model.Usuario;
import com.keoma.ioasys.model.Voto;
import com.keoma.ioasys.repository.FilmeRepository;
import com.keoma.ioasys.repository.UsuarioRepository;
import com.keoma.ioasys.repository.VotoRepository;
import com.keoma.ioasys.service.VotoService;

@Service
public class VotoServiceImpl implements VotoService {

	@Autowired
	private VotoRepository repository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private FilmeRepository filmeRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<Voto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Voto save(VotoSaveDto votoDto) {

		Voto voto = modelMapper.map(votoDto, Voto.class);

		Usuario usuario = usuarioRepository.findById(votoDto.getUsuario().getId())
				.orElseThrow(() -> new UsuarioNaoEncontradoException());

		Filme filme = filmeRepository.findById(votoDto.getFilme().getId())
				.orElseThrow(() -> new FilmeNaoEncontradoException());

		repository.findByUsuarioAndFilme(usuario, filme).ifPresent(v -> voto.setId(v.getId()));

		voto.setUsuario(usuario);

		voto.setFilme(filme);

		Voto retorno = repository.save(voto);

		return retorno;
	}
}
