package com.keoma.ioasys.service.impl;

import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.keoma.ioasys.controller.dto.CreateFilmeDto;
import com.keoma.ioasys.controller.dto.FilmeDto;
import com.keoma.ioasys.controller.dto.IntegranteDto;
import com.keoma.ioasys.exception.UsuarioNaoEncontradoException;
import com.keoma.ioasys.model.Filme;
import com.keoma.ioasys.model.Integrante;
import com.keoma.ioasys.model.enums.GeneroEnum;
import com.keoma.ioasys.repository.FilmeRepository;
import com.keoma.ioasys.service.FilmeService;

@Component
public class FilmeServiceImpl implements FilmeService {

	@Autowired
	private FilmeRepository repository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public Page<FilmeDto> findByFilter(String nome, String descricao, GeneroEnum genero, String[] integrantes, int page, int pageSize) {

		Pageable paginacao = PageRequest.of(page, pageSize, Sort.by("nome").ascending());
		
		Page<Filme> filmes = repository.findByFilter(nome, descricao, genero, paginacao);

		if (filmes.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}
		
		Page<FilmeDto> retorno = new PageImpl<FilmeDto>(filmes.stream().map(this::convertToDto).collect(Collectors.toList()));
						
		return retorno;
	}

	@Override
	public FilmeDto findById(Long id) {

		Optional<Filme> filmeOpt = repository.findById(id);

		if (filmeOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		return modelMapper.map(filmeOpt.get(), FilmeDto.class);
	}

	@Override
	public FilmeDto create(CreateFilmeDto filmeDto) {

		Filme filme = modelMapper.map(filmeDto, Filme.class);

		Filme retorno = repository.save(filme);

		return modelMapper.map(retorno, FilmeDto.class);
	}

	@Override
	public FilmeDto update(Long id, FilmeDto filmeDto) {

		Optional<Filme> filmeOpt = repository.findById(id);

		if (filmeOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		Filme filme = filmeOpt.get();

		filme.setNome(filmeDto.getNome());
		filme.setDescricao(filmeDto.getDescricao());
		filme.setGenero(filmeDto.getGenero());

		filme.setIntegrantes(filmeDto.getIntegrantes().stream().map(this::convertFromDto).collect(Collectors.toList()));

		Filme retorno = repository.save(filme);

		return modelMapper.map(retorno, FilmeDto.class);
	}

	@Override
	public void delete(Long id) {

		Optional<Filme> filmeOpt = repository.findById(id);

		if (filmeOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		repository.deleteById(id);

	}

	private Integrante convertFromDto(IntegranteDto post) {
		Integrante integrante = modelMapper.map(post, Integrante.class);
		return integrante;
	}

	private FilmeDto convertToDto(Filme post) {
		FilmeDto filmeDto = modelMapper.map(post, FilmeDto.class);
		return filmeDto;
	}
}
