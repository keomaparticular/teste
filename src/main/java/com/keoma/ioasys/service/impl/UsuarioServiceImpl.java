package com.keoma.ioasys.service.impl;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.keoma.ioasys.controller.dto.CreateUsuarioDto;
import com.keoma.ioasys.controller.dto.UsuarioDto;
import com.keoma.ioasys.exception.UsuarioNaoEncontradoException;
import com.keoma.ioasys.model.Usuario;
import com.keoma.ioasys.model.enums.RoleEnum;
import com.keoma.ioasys.repository.UsuarioRepository;
import com.keoma.ioasys.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private ModelMapper modelMapper;


	@Override
	public Page<Usuario> findUsuariosAtivos(int page, int pageSize) {

		Pageable paginacao = PageRequest.of(page, pageSize, Sort.by("nome").ascending());

		Page<Usuario> usuarios = repository.findUsuariosAtivos(RoleEnum.ROLE_USUARIO.toString(), paginacao);

		if (usuarios.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		return usuarios;
	}

	@Override
	public Usuario findById(Long id) {

		Optional<Usuario> usuarioOpt = repository.findById(id);

		if (usuarioOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		return usuarioOpt.get();
	}

	@Override
	public Usuario create(CreateUsuarioDto usuarioDto) {

		Usuario usuario = modelMapper.map(usuarioDto, Usuario.class);

		Usuario retorno = repository.save(usuario);

		return retorno;
	}

	@Override
	public Usuario update(Long id, UsuarioDto usuarioDto) {

		Optional<Usuario> usuarioOpt = repository.findById(id);

		if (usuarioOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		Usuario usuario = usuarioOpt.get();

		usuario.setNome(usuarioDto.getNome());
		usuario.setEmail(usuarioDto.getEmail());
		usuario.setSenha(usuarioDto.getSenha());
		Usuario retorno = repository.save(usuario);

		return retorno;
	}

	@Override
	public void delete(Long id) {

		Optional<Usuario> usuarioOpt = repository.findById(id);

		if (usuarioOpt.isEmpty()) {
			throw new UsuarioNaoEncontradoException();
		}

		Usuario usuario = usuarioOpt.get();
		usuario.setAtivo(false);

		repository.save(usuario);

	}
}