package com.keoma.ioasys.service;

import org.springframework.data.domain.Page;

import com.keoma.ioasys.controller.dto.CreateFilmeDto;
import com.keoma.ioasys.controller.dto.FilmeDto;
import com.keoma.ioasys.model.enums.GeneroEnum;

public interface FilmeService {
	
	public Page<FilmeDto> findByFilter(String nome, String descricao, GeneroEnum genero, String[] integrantes, int page, int pageSize);
	public FilmeDto findById(Long id);
	public FilmeDto create(CreateFilmeDto filmeDto);
	public FilmeDto update(Long id, FilmeDto filmeDto);
	public void delete(Long id);

}
