package com.keoma.ioasys.service;

import org.springframework.data.domain.Page;

import com.keoma.ioasys.controller.dto.CreateUsuarioDto;
import com.keoma.ioasys.controller.dto.UsuarioDto;
import com.keoma.ioasys.model.Usuario;

public interface UsuarioService {
	
	public Page<Usuario> findUsuariosAtivos(int page, int pageSize);
	public Usuario findById(Long id);
	public Usuario create(CreateUsuarioDto usuarioDto);
	public Usuario update(Long id, UsuarioDto usuarioDto);
	public void delete(Long id);
}
