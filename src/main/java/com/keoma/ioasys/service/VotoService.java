package com.keoma.ioasys.service;

import java.util.List;

import com.keoma.ioasys.controller.dto.VotoSaveDto;
import com.keoma.ioasys.model.Voto;

public interface VotoService {
	public List<Voto> findAll();
	public Voto save(VotoSaveDto votoDto);
}
