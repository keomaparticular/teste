package com.keoma.ioasys.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class UsuarioNaoAutorizadoException extends BaseException {
	private static final long serialVersionUID = 476544204685902977L;

	private final String code = "usuarioNaoAutorizadoException";
	private final String message = "Usuário e/ou Senha inválidos.";
	private final HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
}
