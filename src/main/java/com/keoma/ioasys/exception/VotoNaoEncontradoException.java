package com.keoma.ioasys.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class VotoNaoEncontradoException extends BaseException {

	private static final long serialVersionUID = 1L;
	private final String code = "votoNaoEncontradoException";
	private final String message = "Voto não encontrado.";
	private final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
