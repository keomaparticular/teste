package com.keoma.ioasys.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class FilmeNaoEncontradoException extends BaseException {
	private static final long serialVersionUID = -6297118499836597209L;

	private final String code = "filmeNaoEncontradoException";
	private final String message = "Filme não encontrado.";
	private final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
