package com.keoma.ioasys.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public final class ExceptionJson {

	private final String code;
	private final String message;
	private HttpStatus httpStatus;
	
	public ExceptionJson(final BaseException baseException) {
		this.code = baseException.getCode();
		this.message = baseException.getMessage();
		this.httpStatus = baseException.getHttpStatus();
	}
}
