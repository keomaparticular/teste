package com.keoma.ioasys.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.keoma.ioasys.model.enums.GeneroEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Filme {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private String descricao;
	private GeneroEnum genero;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "filme")
	private List<Voto> votos;
	
	@ManyToMany()
	private List<Integrante> integrantes = new ArrayList<>();

}
