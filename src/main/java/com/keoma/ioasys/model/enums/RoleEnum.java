package com.keoma.ioasys.model.enums;

public enum RoleEnum {
	ROLE_USUARIO ("USUARIO"),
	ROLE_ADM ("ADM");
	
	private String descricao;
	
	private RoleEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
