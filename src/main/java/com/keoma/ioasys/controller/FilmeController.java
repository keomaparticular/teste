package com.keoma.ioasys.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.keoma.ioasys.controller.dto.CreateFilmeDto;
import com.keoma.ioasys.controller.dto.FilmeDto;
import com.keoma.ioasys.model.enums.GeneroEnum;
import com.keoma.ioasys.service.FilmeService;

@RestController
@RequestMapping("/filmes")
public class FilmeController {

	@Autowired
	private FilmeService filmeService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public ResponseEntity<?> findByFilter(
			@RequestParam(name = "nome", required = false) String nome,
			@RequestParam(name = "descricao", required = false) String descricao,
			@RequestParam(name = "genero", required = false) GeneroEnum genero,
			@RequestParam(name = "integrantes", required = false) String[] integrantes,
			@RequestParam(name = "page", required = true) int page,
			@RequestParam(name = "pageSize", required = true) int pageSize
			) {
		try {

			final Page<FilmeDto> filmes = filmeService.findByFilter(nome, descricao, genero, integrantes, page, pageSize);

			return ResponseEntity.ok().body(filmes);

		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable(required = true) @Valid final Long id) {

		final FilmeDto filme = filmeService.findById(id);

		return ResponseEntity.ok(filme);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity<?> create(@RequestBody(required = true) @Valid final CreateFilmeDto filmeDto) {

		final FilmeDto filme = filmeService.create(filmeDto);

		return ResponseEntity.ok().body(filme);
	}

	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody(required = true) @Valid final FilmeDto filmeDto,
			@PathVariable(required = true) @Valid final Long id) {

		final FilmeDto filme = filmeService.update(id, filmeDto);

		return ResponseEntity.ok().body(filme);
	}

	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<?> delete(@PathVariable long id) {

		filmeService.delete(id);

		return ResponseEntity.ok().build();
	}

}
