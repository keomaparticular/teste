package com.keoma.ioasys.controller.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseVotoDto {
	private Long id;
	private Long valor;
	private ResponseUsuarioDto usuario;
	private ResponseFilmeDto filme;
}
