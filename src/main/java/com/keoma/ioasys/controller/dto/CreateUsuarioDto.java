package com.keoma.ioasys.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateUsuarioDto {	
	private String nome;
	private String email;
	private String senha;
	private boolean ativo;
}
