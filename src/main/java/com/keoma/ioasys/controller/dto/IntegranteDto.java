package com.keoma.ioasys.controller.dto;

import com.keoma.ioasys.model.enums.PerfilIntegranteEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IntegranteDto {
	private Long id;
	private String nome;
	private PerfilIntegranteEnum perfil;
}
