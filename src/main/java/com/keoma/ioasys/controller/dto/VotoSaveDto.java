package com.keoma.ioasys.controller.dto;

import com.keoma.ioasys.model.Filme;
import com.keoma.ioasys.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VotoSaveDto {
	private Long id;
	private Long valor;
	private Usuario usuario;
	private Filme filme;
}
