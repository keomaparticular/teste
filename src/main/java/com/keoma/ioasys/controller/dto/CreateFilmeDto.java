package com.keoma.ioasys.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.keoma.ioasys.model.enums.GeneroEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateFilmeDto {
	private String nome;
	private String descricao;
	private GeneroEnum genero;
	private List<IntegranteDto> integrantes = new ArrayList<>();
}
