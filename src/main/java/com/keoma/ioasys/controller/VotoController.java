package com.keoma.ioasys.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.keoma.ioasys.controller.dto.VotoSaveDto;
import com.keoma.ioasys.model.Voto;
import com.keoma.ioasys.service.VotoService;

@RestController
@RequestMapping("/votos")
public class VotoController {

	@Autowired
	private VotoService votoService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public ResponseEntity<?> findAll() {
		try {

			final List<Voto> votos = votoService.findAll();

			return ResponseEntity.ok().body(votos);

		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@ResponseStatus(HttpStatus.OK)
	@PostMapping
	public ResponseEntity<?> save(@RequestBody(required = true) @Valid final VotoSaveDto votoDto) {

		final Voto voto = votoService.save(votoDto);

		return ResponseEntity.ok().body(voto);
	}
}
