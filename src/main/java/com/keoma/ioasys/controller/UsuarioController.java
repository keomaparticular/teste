package com.keoma.ioasys.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.keoma.ioasys.controller.dto.CreateUsuarioDto;
import com.keoma.ioasys.controller.dto.UsuarioDto;
import com.keoma.ioasys.model.Usuario;
import com.keoma.ioasys.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable(required = true) @Valid final Long id) {

		final Usuario usuario = usuarioService.findById(id);

		return ResponseEntity.ok(usuario);
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/ativos")
	public ResponseEntity<?> findUsuariosAtivos(
			@RequestParam(name = "page", required = true) int page,
			@RequestParam(name = "pageSize", required = true) int pageSize) {

		final Page<Usuario> usuarios = usuarioService.findUsuariosAtivos(page, pageSize);

		return ResponseEntity.ok(usuarios);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity<?> create(@RequestBody(required = true) @Valid final CreateUsuarioDto usuarioDto) {

		final Usuario usuario = usuarioService.create(usuarioDto);

		return ResponseEntity.ok().body(usuario);
	}

	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody(required = true) @Valid final UsuarioDto usuarioDto,
			@PathVariable(required = true) @Valid final Long id) {

		final Usuario usuario = usuarioService.update(id, usuarioDto);

		return ResponseEntity.ok().body(usuario);
	}

	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<?> delete(@PathVariable long id) {

		usuarioService.delete(id);

		return ResponseEntity.ok().build();
	}

}
